export class Comment 
{
    IdComment? : number;
    title : string;
    description : string;
    creation : Date;
}