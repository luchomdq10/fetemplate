import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CommentServiceService {

  private API:string = "https://localhost:44318/"

  constructor(private http:HttpClient) { }

  public GetComments():Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(this.API + "Comment",{headers:headers}).toPromise();
  }

  public GetCommentById(idComment:number):Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(this.API + "Comment/"+ idComment,{headers:headers}).toPromise();
  }
}
