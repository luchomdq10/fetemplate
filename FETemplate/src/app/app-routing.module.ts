import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddEditComponentComponent } from './components/add-edit-component/add-edit-component.component';
import { DetailsCommentComponent } from './components/details-comment/details-comment.component';
import { ListComponentsComponent } from './components/list-components/list-components.component';


const routes: Routes = [
  {path:'create', component:AddEditComponentComponent},
  {path:'edit/:id', component:AddEditComponentComponent},
  {path:'detail/:id', component:DetailsCommentComponent},
  {path:'', component: ListComponentsComponent, pathMatch: 'full'},
  {path:'**', redirectTo: '/'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
