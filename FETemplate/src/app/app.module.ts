import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AddEditComponentComponent } from './components/add-edit-component/add-edit-component.component';
import { ListComponentsComponent } from './components/list-components/list-components.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DetailsCommentComponent } from './components/details-comment/details-comment.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    AddEditComponentComponent,
    ListComponentsComponent,
    NavbarComponent,
    DetailsCommentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
