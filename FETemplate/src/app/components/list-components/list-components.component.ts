import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommentServiceService } from 'src/app/services/comments/comment-service.service';

@Component({
  selector: 'app-list-components',
  templateUrl: './list-components.component.html',
  styleUrls: ['./list-components.component.css']
})
export class ListComponentsComponent implements OnInit {

  constructor(private router: Router, private commentServiceService:CommentServiceService) { }
  
  comments:Array<Comment> = new Array<Comment>();

  ngOnInit()
  {
    this.commentServiceService.GetComments().then(response => {
      this.comments = response;
    }).catch(response => {
      console.log(response);
    });
  }
}
